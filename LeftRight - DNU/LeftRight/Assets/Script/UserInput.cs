﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour
{

    //Need access to the auto runner then change the bools to change the direction of the player
    private AutoRunAxis autoRunner = null;

    private TileMapGenerator tileMap = null;

    // Use this for initialization
    void Start ()
    {
        autoRunner = GetComponent<AutoRunAxis>(); 
        autoRunner.X_Axis = true;


        tileMap = GameObject.FindGameObjectWithTag("TileManager").GetComponent<TileMapGenerator>();
    }
 

    //Main update loop - Needs to be Update
    void Update ()
    {

        if (Input.GetMouseButtonDown(0))//Left Click
        {
            Debug.Log("Left Click!");
            autoRunner.Positive = true;
        }

        if (Input.GetMouseButtonDown(1))//Right Click
        {
            Debug.Log("Right Click!");
            autoRunner.Positive = false;
        }

        if (Input.GetMouseButtonDown(2))//Middle Click
        {
            Debug.Log("Middle Click!");
            autoRunner.Positive = !autoRunner.Positive;

            autoRunner.X_Axis = !autoRunner.X_Axis;
            autoRunner.Y_Axis = !autoRunner.Y_Axis;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            tileMap.AddTile();


            tileMap.RemoveTile();
        }
    }



}
