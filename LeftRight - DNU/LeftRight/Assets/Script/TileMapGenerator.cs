﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class TileMapGenerator : MonoBehaviour {

    public GameObject tilePrefab = null;
    public GameObject parentObject = null;

  
    private Direction[] row1_Directions = new Direction[] { Direction.FORWARD, Direction.RIGHT };
    private Direction[] row2_Directions = new Direction[] { Direction.RIGHT, Direction.LEFT, Direction.FORWARD };
    private Direction[] row3_Directions = new Direction[] { Direction.LEFT, Direction.FORWARD };
    

    private Transform previousTile;
    private Direction randomDirection;
    private Direction previousDirection;

    //[SerializeField]
    private List<Transform> Tiles = null;


        
    int lastRowUsed = 0;
    /// <summary>
    /// Find the last row used and have that as the current row. 
    /// </summary>
    public void AddTile()
    {
        randomDirection = PickRandomDirection(lastRowUsed);

        TilePlace(ref lastRowUsed, randomDirection);
    }

    //Removes the first tile from the list
    public void RemoveTile()
    {
        Destroy(Tiles[0].gameObject);
        Tiles.RemoveAt(0);
    }

    private void Awake ()
    {
        Tiles = GetComponentInChildren<TileManager>().Tiles;


        int currentRow = 2;//Middle Row

        GameObject startTile = Instantiate(tilePrefab, new Vector3(0, 0, 0), this.transform.rotation);//Starting Tile

        startTile.transform.parent = parentObject.transform;

        Tiles.Add(startTile.transform);//Adding the start tile to the Tiles list

        previousTile = startTile.transform;//update the previous tile

        randomDirection = Direction.FORWARD;
        
        for (int index = 1; index < 25; index++)
        {
         
            randomDirection = PickRandomDirection(currentRow);
               
            TilePlace(ref currentRow, randomDirection);

        }

        lastRowUsed = currentRow;
    }
    /// <summary>
    /// This will pick a random direction for the next tile, calculating a new posistion in a way.
    /// </summary>
    /// <param name="currentRow"> Keeping track on what row it is in, valid row values(1,2,3) </param>
    /// <param name="index"> Current loop count </param>
    private Direction PickRandomDirection(int currentRow)
    {
        //DIRECTION
        //work out which row last tile was in and calculate the next tile's direction
        if (currentRow == 1)
        {
            //generate random direction using row1_Directions
            int directionSelection = Random.Range(0, row1_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;

            //Assign the next direction a random direction fron the currentRow
            randomDirection = row1_Directions[directionSelection];

            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);

        }
        else if (currentRow == 2)
        {
            //generate random direction using row2_Directions
            int directionSelection = Random.Range(0, row2_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;

            //Assign the next direction a random direction fron the currentRow
            randomDirection = row2_Directions[directionSelection];

            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);
        }
        else if (currentRow == 3)
        {
            //generate random direction using row3_Directions
            int directionSelection = Random.Range(0, row3_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;

            //Assign the next direction a random direction fron the currentRow
            randomDirection = row3_Directions[directionSelection];

            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);
        }
        else
        {
            //something went wrong
            Debug.Log("WTF", this);
        }

        return randomDirection;

    }



    /// <summary>
    /// Placing a tile at in the row selected by the PickRandomDirection.
    /// </summary>
    /// <param name="rowNumber"> Keeping track on what row it is in, valid row values(1,2,3) </param>
    private void TilePlace(ref int rowNumber, Direction nextDirection)
    {
        GameObject newTile = null;
        ////spawn a new tile in that direction AND update row
        if (nextDirection == Direction.FORWARD || (nextDirection == Direction.LEFT && previousDirection == Direction.RIGHT) || (nextDirection == Direction.RIGHT && previousDirection == Direction.LEFT))
        {
            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x + 2.54f, 0, previousTile.transform.position.z), this.transform.rotation);//Straight along X+
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile.transform;//update the previous tile

        }
        else if (nextDirection == Direction.LEFT)
        {

            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x /*+ 2.54f*/, 0, previousTile.transform.position.z + 2.54f), this.transform.rotation);//Straight along Z+
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile.transform;//update the previous tile

            rowNumber--;//Adjust row

        }
        else if (nextDirection == Direction.RIGHT)
        {

            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x/* + 2.54f*/, 0, previousTile.transform.position.z - 2.54f), this.transform.rotation);//Straight along Z-
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile.transform;//update the previous tile

            rowNumber++;//Adjust row
        }

        //Adding a new tile to the Tiles list
        Tiles.Add(newTile.transform);

    }

}//end of class




public enum Direction
{
    FORWARD,
    LEFT,
    RIGHT
}