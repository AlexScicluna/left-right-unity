﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRunAxis : MonoBehaviour {

	[Range(0.0f, 4.0f)]
	public float Velocity;

    [SerializeField]
    private bool positive;

    [SerializeField]
    private bool x_Axis;
    [SerializeField]
    private bool y_Axis;
    [SerializeField]
    private bool z_Axis;

    //Getters
    public bool X_Axis
    {
        get { return x_Axis; }
        set { x_Axis = value; }
    }

    public bool Y_Axis
    {
        get { return y_Axis; }
        set { y_Axis = value; }
    }

    public bool Z_Axis
    {
        get { return z_Axis; }
        set { z_Axis = value; }
    }

    public bool Positive
    {
        get { return positive; }
        set { positive = value; }
    }


    // Use this for initialization
    private void Start ()
	{

	}

    // Update is called once per frame
    private void FixedUpdate ()
	{

		if (x_Axis)
		{
			//Disable the other two axis
			y_Axis = false;
			z_Axis = false;

			if (positive)
			{
				//Move Object along the x_Axis
				this.transform.position += new Vector3(Velocity * Time.deltaTime, 0, 0);
			}
			else
			{
				//Move Object along the x_Axis
				this.transform.position -= new Vector3(Velocity * Time.deltaTime, 0, 0);
			}

		}

		if (y_Axis)
		{
			//Disable the other two axis
			x_Axis = false;
			z_Axis = false;

			if (positive)
			{
				//Move Object along the y_Axis
				this.transform.position += new Vector3(0, Velocity * Time.deltaTime, 0);
			}
			else
			{
				//Move Object along the y_Axis
				this.transform.position -= new Vector3(0, Velocity * Time.deltaTime, 0);
			}
			
		}

		if (z_Axis)
		{
			//Disable the other two axis
			x_Axis = false;
			y_Axis = false;


			if (positive)
			{
				//Move Object along the z_Axis
				this.transform.position += new Vector3(0, 0, Velocity * Time.deltaTime);
			}
			else
			{
				//Move Object along the z_Axis
				this.transform.position -= new Vector3(0, 0, Velocity * Time.deltaTime);
			}


		}

	}//FixedUpdate



}
