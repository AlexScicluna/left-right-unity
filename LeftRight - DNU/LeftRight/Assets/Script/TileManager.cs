﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[ExecuteInEditMode]
public class TileManager : MonoBehaviour {



    public List<Transform> Tiles = new List<Transform>();

    private int index;

    // Use this for initialization
    void Start ()
    {
        
    }
    
    // Update is called once per frame
    void Update ()
    {
        Transform[] tiles = GetComponentsInChildren<Transform>();

        if (tiles.Length > 0)
        {
            //Empty the list and add them back in.
            Tiles.Clear();

            index = 0;

            foreach (Transform transform in tiles)
            {
                //Increment if not this object that it is attached to
                if (transform != this.transform)
                {
                    index++;
                }

                if (index == 0)
                {
                    transform.name = "Tile_Manager";
                }
                else
                {
                    transform.name = "Tile_" + index;
                    //Adds it to the List of Tiles
                    Tiles.Add(transform);
                }

             

            }


        }

            
    }



    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        foreach (Transform transform in Tiles)
        {
            Gizmos.DrawSphere(transform.position, 0.25f);
        }

        Gizmos.color = Color.yellow;
        for (int index = 0; index < Tiles.Count - 1; index++)
        {
            Gizmos.DrawLine(Tiles[index].position, Tiles[index + 1].position);

            //Gizmos.DrawLine(Tiles[0].position, Tiles[Tiles.Count - 1].position);//End to start
        }
    }

}
