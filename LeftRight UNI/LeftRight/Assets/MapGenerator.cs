﻿using UnityEngine;
using System.Collections;

public class MapGenerator : MonoBehaviour {
	public int mapRows = 3;
	public int mapColumns = 3;

	public char[,] map;

	public string boxCharacters;
	private string[] boxCharacterUpFriends;
	private string[] boxCharacterDownFriends;
	private string[] boxCharacterLeftFriends;
	private string[] boxCharacterRightFriends;

	private RoomGenerator roomGenerator;

	// Use this for initialization
	void Awake () {
		roomGenerator = GetComponent<RoomGenerator> ();
		InitializeBoxCharacters ();
	}



	
	// Update is called once per frame
	void Update () {
 
	}

	public void DisplayMap() {
		string output = "";
		for (int r = 0; r < mapRows; r++) {
			for (int c = 0; c < mapColumns; c++) {
				output += map [r, c];
			}
			output += "\n";
		}
		Debug.Log (output);
	}

	public void InitializeMap() {
		map = new char[mapRows, mapColumns];

		// Put 'X's in top and bottom rows.
		for (int c = 0; c < mapColumns; c++) {
			map [0, c] = 'X';
			map [mapRows - 1, c] = 'X';
		}

		// Put 'X's in the left and right columns.
		for (int r = 0; r < mapRows; r++) {
			map [r, 0] = 'X';
			map [r, mapColumns - 1] = 'X';
		}

		// Set 'O' for the other map spaces (which means 'free').
		for (int r = 1; r < mapRows - 1; r++) {
			for (int c = 1; c < mapColumns - 1; c++) {
				map [r, c] = 'O';
			}
		}

        
        int mapSeed = System.DateTime.Now.Millisecond;  // Testing Seeds:   System.DateTime.Now.Millisecond; 
		//CheckBannedSeeds(mapSeed);


		//Debug.Log ("Current seed = " + mapSeed);

		roomGenerator.RandomRoomGeneration ();
	
		for (int r = 1; r < mapRows - 1; r++) {
			for (int c = 1; c < mapColumns - 1; c++) {

				if (map [r, c] == '@' || map[r,c] == '#' || map [r, c] == '˄' || map [r, c] == '˂' || map [r, c] == '˃' || map [r, c] == '˅' ||
					map[r,c] == '╔' || map[r,c] == '╧' || map[r,c] == '╤' || map[r,c] == '╗' || map[r,c] == '╟' || map[r,c] == '╢' || map[r,c] == '╚' || map[r,c] == '╝' || 
					map[r,c] == '►' || map[r,c] == '◄' || map[r,c] == '▲' || map[r,c] == '▼') {
					continue;
				}

				string validCharacters = GetValidBoxCharacters (r, c);
				map [r, c] = validCharacters [Random.Range (0, validCharacters.Length)];
			}
		}

		SortOutDeadEnds ();

		LinkToStartArea ();

        // X X X X X X X X X X X X 
        // X ┬ ┐ ┌ ─ ─ ┐ ┌ ─ ┬ ┐ X 
        // X │ ├ ┴ ┬ ┬ ┘ └ ┬ ┼ ┤ X 
        // X ├ ┼ ┐ └ ┘ ► ─ ┘ └ ┘ X 
        // X ├ ┤ │ ╔ ╤ ╤ ╤ ╤ ╗ ▼ X 
        // X ├ ┘ ├ ˂ @ @ # @ ˃ ┤ X 
        // X └ ┐ │ ╚ ╧ ╧ ╧ ╧ ╝ │ X 
        // X ┌ ┘ └ ─ ─ ┐ ┌ ┬ ┐ │ X 
        // X └ ┬ ┬ ─ ┐ │ ├ ┤ └ ┤ X 
        // X ┌ ┤ ├ ┬ ┤ └ ┤ ├ ┬ ┤ X 
        // X └ ┴ ┘ └ ┘ ► ┘ └ ┘ ▲ X 
        // X X X X X X X X X X X X 

	}

 
	private void LinkToStartArea() {
			
		map [1, 1] = '┬';

		Debug.Log ("Created link to start area");
	}
 

	 
	//private int CheckBannedSeeds(int mapSeed) {
	//	//Add more seeds later and use OR logic
	//	if (mapSeed == 600)// || mapSeed == ??)
	//	{
	//		//ReRoll
	//		int newMapSeed =  System.DateTime.Now.Millisecond; 
	//		Random.InitState(newMapSeed);
	//	}
	//	Debug.Log ("Banned Seed detected and picked another.");
	//	return mapSeed;
	//}

	 

	private void SortOutDeadEnds() {
		// ▼ ▲ ◄ ►
		for (int r = 1; r < mapRows - 1; r++) {
			for (int c = 1; c < mapColumns - 1; c++) {
				if (map [r, c] == 'O') {
					//Debug.Log ("Found dead end at " + r + "," + c);

					if ("─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ()) && !"─┌└├┬┴┼˃".Contains (map [r, c - 1].ToString ()) && !"│┌┐├┤┬┼˅".Contains (map [r - 1, c].ToString ()) && !"│└┘├┤┴┼˄".Contains (map [r + 1, c].ToString ())){
						//RIGHT_!Left-!Top-!Bottom
						//Debug.Log ("dead_end Right at " + r + "," + c);
						map [r, c] = '►';
					
					} else if ("─┌└├┬┴┼˃".Contains (map [r, c - 1].ToString ()) && !"│┌┐├┤┬┼˅".Contains (map [r - 1, c].ToString ()) && !"─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ()) && !"│└┘├┤┴┼˄".Contains (map [r + 1, c].ToString ())){
						//LEFT_!Top-!Right-!Bottom
						//Debug.Log ("dead_end Left at " + r + "," + c);
						map [r, c] = '◄';

					} else if ("│┌┐├┤┬┼˅".Contains (map [r - 1, c].ToString ()) && !"─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ()) && !"─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ()) && !"│└┘├┤┴┼˄".Contains (map [r + 1, c].ToString ())){
						//TOP_!Left-!Right-!Bottom
						//Debug.Log ("dead_end Top at " + r + "," + c);
						map [r, c] = '▲';

					} else if ("│└┘├┤┴┼˄".Contains (map [r + 1, c].ToString ()) && !"─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ()) && !"│┌┐├┤┬┼˅".Contains (map [r - 1, c].ToString ()) && !"─┐┘┤┬┴┼˂".Contains (map [r, c + 1].ToString ())){
						//DOWN_!Left-!Top-!Right
						//Debug.Log ("dead_end Bottom at " + r + "," + c);
						map [r, c] = '▼';


						//Special Spot fix?
					} else if ("┤┼˄┴├│└┘".Contains (map [r + 1, c].ToString ())){
						Debug.Log ("Down to Up! Special spot at " + r + "," + c);
						map [r, c] = '┌';

					} else if ("┼┴├└─┌˃".Contains (map [r, c - 1].ToString ())){
						Debug.Log ("Left to Right! Special spot at " + r + "," + c);
						map [r, c] = '┐';

					} else if ("┤┼├˅┐┬┌".Contains (map [r - 1, c].ToString ())){
						Debug.Log ("Up to Down! Special spot at " + r + "," + c);
						map [r, c] = '└';
					
					} else if ("┐┼┬┘┤┴˂─".Contains (map [r, c + 1].ToString ())){
						Debug.Log ("Right to Left! Special spot at " + r + "," + c);
						map [r, c] = '┌';

					} else {
						Debug.Log ("really_Special Spot! at " + r + "," + c);
					}
				}
			}
		}
	}



	private string GetValidBoxCharacters(int row, int column) {
		string validCharacters = "";

		for (int i = 0; i < boxCharacters.Length; i++) {
			char ch = boxCharacters [i];

			if (boxCharacterLeftFriends [i].Contains (map [row, column - 1].ToString ()) &&
				boxCharacterRightFriends [i].Contains (map [row, column + 1].ToString ()) &&
				boxCharacterUpFriends [i].Contains (map [row - 1, column].ToString ()) &&
				boxCharacterDownFriends [i].Contains (map [row + 1, column].ToString ()))
			{
				validCharacters += ch.ToString ();
			}
		}

		if (validCharacters.Length == 0) {
			validCharacters = "O";
		}

		return validCharacters;
	}//End of GetValidBoxCharacters

	public bool[,] TraverseMap() {
		bool[,] visitedCells = new bool[mapRows, mapColumns];
		int currentRow = 1;
		int currentColumn = 1;

		// This will start the recursive loop, updating the visitedCells array.
		TraverseCells (visitedCells, currentRow, currentColumn);

		return visitedCells;
	}


	private void TraverseCells(bool[,] visitedCells, int row, int column) {
		//row = horizontal
		//columns = vertical
		if (visitedCells [row, column]) {
			return;
		}

		visitedCells [row, column] = true;

		switch (map [row, column]) {
		case '┌'://Corner
		case '╔'://Room Corner
			TraverseCells (visitedCells, row, column + 1);
			TraverseCells (visitedCells, row + 1, column);
			break;
		case '┐'://Corner
		case '╗'://Room Corner
			TraverseCells (visitedCells, row + 1, column);
			TraverseCells (visitedCells, row, column - 1);
			break;
		case '─'://Horizontal Straight
		case '˂'://Left Room Entry
		case '˃'://Right Room Entry
			TraverseCells (visitedCells, row, column - 1);
			TraverseCells (visitedCells, row, column + 1);
			break;
		case '│'://Vertial Straight
		case '˄'://Up Room Entry
		case '˅'://Down Room Entry
			TraverseCells (visitedCells, row - 1, column);
			TraverseCells (visitedCells, row + 1, column);
			break;
		case '└'://Corner
		case '╚'://Room Corner
			TraverseCells (visitedCells, row, column + 1);
			TraverseCells (visitedCells, row - 1, column);
			break;
		case '┘'://Corner
		case '╝'://Room Corner
			TraverseCells (visitedCells, row - 1, column);
			TraverseCells (visitedCells, row, column - 1);
			break;
		case '├'://T-Intersection
		case '╟'://RoomWall
			TraverseCells (visitedCells, row - 1, column);
			TraverseCells (visitedCells, row + 1, column);
			TraverseCells (visitedCells, row, column + 1);
			break;
		case '┤'://T-Intersection
		case '╢'://RoomWall
			TraverseCells (visitedCells, row - 1, column);
			TraverseCells (visitedCells, row + 1, column);
			TraverseCells (visitedCells, row, column - 1);
			break;
		case '┬'://T-Intersection
		case '╤'://RoomWall
			TraverseCells (visitedCells, row, column - 1);
			TraverseCells (visitedCells, row, column + 1);
			TraverseCells (visitedCells, row + 1, column);
			break;
		case '┴'://T-Intersection
		case '╧'://RoomWall
			TraverseCells (visitedCells, row, column - 1);
			TraverseCells (visitedCells, row, column + 1);
			TraverseCells (visitedCells, row - 1, column);
			break;

		case '┼'://CrossRoads
		case '@'://Room
		case '#'://Room_alt/wTrapDoor
			TraverseCells (visitedCells, row, column - 1);
			TraverseCells (visitedCells, row, column + 1);
			TraverseCells (visitedCells, row - 1, column);
			TraverseCells (visitedCells, row + 1, column);
			break;
		
		//Traversing dead ends	
		case '►'://DeadEnd Right
				TraverseCells (visitedCells, row, column + 1);
				break;
		case '◄'://DeadEnd Left
				TraverseCells (visitedCells, row, column- 1);
				break;
		case '▲'://DeadEnd Up
				TraverseCells (visitedCells, row - 1, column);
				break;
		case '▼'://DeadEnd Down
				TraverseCells (visitedCells, row + 1, column);
				break;
		
		//Non-Traversable
		case 'O':// Empty Spots
				return; 
		case 'X':// MazeWall	
				return; 

		//Default Switch Case, stupid special spots. :D			
		default:
			Debug.LogError ("No idea how we got here (" + row + "," + column + ") '" + map[row,column]);
			return;
		}
	}//End of TraverseCells

	private void InitializeBoxCharacters() {
		boxCharacters = "─│┌┐└┘├┤┬┴┼"; 
		boxCharacterUpFriends = new string[boxCharacters.Length];
		boxCharacterDownFriends = new string[boxCharacters.Length];
		boxCharacterLeftFriends = new string[boxCharacters.Length];
		boxCharacterRightFriends = new string[boxCharacters.Length];

		//D▼ U▲ L◄ R► 
		// ˂ ˃ ˅ ˄
		// ╟ ╢ ╤ ╧ ╔ ╗ ╚ ╝
		boxCharacterLeftFriends [0] = "O─┌└├┬┴┼►"; //─
		boxCharacterLeftFriends [1] = "O│┐┘┤X╟╢╤╧╔╗╚╝"; //│
		boxCharacterLeftFriends [2] = "O│┐┘┤X╟╢╤╧╔╗╚╝"; //┌
		boxCharacterLeftFriends [3] = "O─┌└├┬┴┼►"; //┐
		boxCharacterLeftFriends [4] = "O│┐┘┤X╟╢╤╧╔╗╚╝"; //└
		boxCharacterLeftFriends [5] = "O─┌└├┬┴┼►"; //┘
		boxCharacterLeftFriends [6] = "O│┐┘┤X╟╢╤╧╔╗╚╝"; //├
		boxCharacterLeftFriends [7] = "O─┌└├┬┴┼˃►"; //┤
		boxCharacterLeftFriends [8] = "O─┌└├┬┴┼˃►"; //┬
		boxCharacterLeftFriends [9] = "O─┌└├┬┴┼˃►"; //┴
		boxCharacterLeftFriends [10] = "O─┌└├┬┴┼˃►"; //┼

		//boxCharacterLeftFriends [11] = "O"; //►
	
		boxCharacterRightFriends [0] = "O─┐┘┤┬┴┼◄"; //─
		boxCharacterRightFriends [1] = "O│┌└├X╟╢╤╧╔╗╚╝"; //│
		boxCharacterRightFriends [2] = "O─┐┘┤┬┴┼◄"; //┌
		boxCharacterRightFriends [3] = "O│┌└├X╟╢╤╧╔╗╚╝"; //┐
		boxCharacterRightFriends [4] = "O─┐┘┤┬┴┼◄"; //└
		boxCharacterRightFriends [5] = "O│┌└├X╟╢╤╧╔╗╚╝"; //┘
		boxCharacterRightFriends [6] = "O─┐┘┤┬┴┼˂◄"; //├
		boxCharacterRightFriends [7] = "O│┌└├X╟╢╤╧╔╗╚╝"; //┤
		boxCharacterRightFriends [8] = "O─┐┘┤┬┴┼˂◄"; //┬
		boxCharacterRightFriends [9] = "O─┐┘┤┬┴┼˂◄"; //┴
		boxCharacterRightFriends [10] = "O─┐┘┤┬┴┼˂◄"; //┼


		boxCharacterUpFriends [0] = "O─└┘┴X╟╢╤╧╔╗╚╝"; //─
		boxCharacterUpFriends [1] = "O│┌┐├┤┬┼▼"; //│
		boxCharacterUpFriends [2] = "O─└┘┴X╟╢╤╧╔╗╚╝"; //┌
		boxCharacterUpFriends [3] = "O─└┘┴X╟╢╤╧╔╗╚╝"; //┐
		boxCharacterUpFriends [4] = "O│┌┐├┤┬┼▼"; //└
		boxCharacterUpFriends [5] = "O│┌┐├┤┬┼▼"; //┘
		boxCharacterUpFriends [6] = "O│┌┐├┤┬┼˅▼"; //├
		boxCharacterUpFriends [7] = "O│┌┐├┤┬┼˅▼"; //┤
		boxCharacterUpFriends [8] = "O─└┘┴X╟╢╤╧╔╗╚╝"; //┬
		boxCharacterUpFriends [9] = "O│┌┐├┤┬┼˅▼"; //┴
		boxCharacterUpFriends [10] = "O│┌┐├┤┬┼˅▼"; //┼


		boxCharacterDownFriends [0] = "O─┌┐┬X╟╢╤╧╔╗╚╝"; //─
		boxCharacterDownFriends [1] = "O│└┘├┤┴┼▲"; //│
		boxCharacterDownFriends [2] = "O│└┘├┤┴┼▲"; //┌
		boxCharacterDownFriends [3] = "O│└┘├┤┴┼▲"; //┐
		boxCharacterDownFriends [4] = "O─┌┐┬X╟╢╤╧╔╗╚╝"; //└
		boxCharacterDownFriends [5] = "O─┌┐┬X╟╢╤╧╔╗╚╝"; //┘
		boxCharacterDownFriends [6] = "O│└┘├┤┴┼˄▲"; //├
		boxCharacterDownFriends [7] = "O│└┘├┤┴┼˄▲"; //┤
		boxCharacterDownFriends [8] = "O│└┘├┤┴┼˄▲"; //┬
		boxCharacterDownFriends [9] = "O─┌┐┬X╟╢╤╧╔╗╚╝"; //┴
		boxCharacterDownFriends [10] = "O│└┘├┤┴┼˄▲"; //┼
	}//End of InitializeBoxCharacters

}//End of MapGenerator Class
