﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowAxis : MonoBehaviour {


    public GameObject target;

    private float Velocity;

    [SerializeField]
    private bool x_Axis;
    [SerializeField]
    private bool y_Axis;
    [SerializeField]
    private bool z_Axis;

    //Properties
    public bool X_Axis
    {
        get { return x_Axis; }
        set { x_Axis = value; }
    }

    public bool Y_Axis
    {
        get { return y_Axis; }
        set { y_Axis = value; }
    }

    public bool Z_Axis
    {
        get { return z_Axis; }
        set { z_Axis = value; }
    }

 

    // Use this for initialization
    private void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");//Find the player
         
        Velocity = target.GetComponent<AutoRunAxis>().Velocity;//Matches the targets auto run speed
    }
   
 
    // Update is called once per frame
    private void FixedUpdate()
    {

        if (x_Axis)
        {
            //Disable the other two axis
            y_Axis = false;
            z_Axis = false;

            //Move Object along the x_Axis
            this.transform.position = new Vector3(target.transform.position.x, 4.0f, this.transform.position.z);
        }
            
        

        if (y_Axis)
        {
            //Disable the other two axis
            x_Axis = false;
            z_Axis = false;

            //Move Object along the y_Axis
            this.transform.position = new Vector3(this.transform.position.x, target.transform.position.y + 4.0f, this.transform.position.z);
        }
          
             

        if (z_Axis)
        {
            //Disable the other two axis
            x_Axis = false;
            y_Axis = false;

            //Move Object along the z_Axis
            this.transform.position = new Vector3(this.transform.position.x, 4.0f, target.transform.position.z);
        }
             
            
    }//FixedUpdate




}
