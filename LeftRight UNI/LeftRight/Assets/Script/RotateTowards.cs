﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateTowards : MonoBehaviour {

	public GameObject tile1 = null;
	public GameObject tile2 = null;



	// Use this for initialization
	void Start ()
	{
		//Vector3 between END FROM START
		Vector3 vecBetweenTiles = tile2.transform.position - tile1.transform.position;

		//Rotate this based of the vectorBetween's Direction/angle
		this.transform.rotation = Quaternion.LookRotation(vecBetweenTiles);
			
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
