﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//1. create a TileInfo class
//2. when generating a Tile at startup, ensure each Tile has a TileInfo script attached

//3. Set each TileInfo's next and previous pointers to the next and previous Tile in the sequence

//4. Get vector between currentTile center and nextTile center, and turn the player in that direction.

public class TileInformation : MonoBehaviour {


    private Transform previousTile = null;
    private Direction previousTileDirection;
    private Transform nextTile = null;
    private Direction nextTileDirection;

    public Direction currentTileDirection;

    private int rowPosition;
    private int colPosition;

    public Transform PreviousTile
    {
        get { return previousTile; }
        set { previousTile = value; }
    }

    public Direction PreviousTileDirection
    {
        get { return previousTileDirection; }
        set { previousTileDirection = value; }
    }

    public Transform NextTile
    {
        get { return nextTile; }
        set { nextTile = value; }
    }

    public Direction NextTileDirection
    {
        get { return nextTileDirection; }
        set { nextTileDirection = value; }
    }


    // Use this for initialization
    private void Start ()
    {
        
    }

    // Update is called once per frame
    private void Update ()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Debug.Log("Current tile: NT: " + NextTileDirection, this);
        }
    }


}
