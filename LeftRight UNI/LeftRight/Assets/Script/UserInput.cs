﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserInput : MonoBehaviour
{

    //Need access to the auto runner then change the bools to change the direction of the player
    private AutoRunAxis autoRunner = null;

    private TileMapGenerator tileMap = null;

    // Use this for initialization
    void Start ()
    {
        autoRunner = GetComponent<AutoRunAxis>(); 
        
        tileMap = GameObject.FindGameObjectWithTag("TileManager").GetComponent<TileMapGenerator>();
    }
 

    //Main update loop - Needs to be Update
    void Update ()
    {

        if (Input.GetMouseButtonDown(0))//Left Click
        {
            Debug.Log("Left Click!");
            autoRunner.Positive = false;
        }

        if (Input.GetMouseButtonDown(1))//Right Click
        {
            Debug.Log("Right Click!");
            autoRunner.Positive = true;
        }

        if (Input.GetMouseButtonDown(2))//Middle Click
        {
            Debug.Log("Middle Click!");
            autoRunner.Positive = !autoRunner.Positive;

            //autoRunner.Z_Axis = !autoRunner.Z_Axis;
            autoRunner.X_Axis = !autoRunner.X_Axis;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            tileMap.AddTile();


            tileMap.RemoveTile();
        }




        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            autoRunner.Positive = true;

            autoRunner.Z_Axis = true;

            autoRunner.X_Axis = false;
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            autoRunner.Positive = false;

            autoRunner.Z_Axis = true;

            autoRunner.X_Axis = false;
        }



        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            autoRunner.Positive = false;

            autoRunner.Z_Axis = false;

            autoRunner.X_Axis = true;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            autoRunner.Positive = true;

            autoRunner.Z_Axis = true;

            autoRunner.X_Axis = true;
        }



    }



}
