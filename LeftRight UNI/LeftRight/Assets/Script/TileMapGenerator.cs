﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
 
public class TileMapGenerator : MonoBehaviour {

    public GameObject tilePrefab = null;
    public GameObject parentObject = null;

  
    private Direction[] row1_Directions = new Direction[] { Direction.FORWARD, Direction.RIGHT };
    private Direction[] row2_Directions = new Direction[] { Direction.RIGHT, Direction.LEFT, Direction.FORWARD };
    private Direction[] row3_Directions = new Direction[] { Direction.LEFT, Direction.FORWARD };
    

    private GameObject previousTile;
    private Direction randomDirection;
    private List<Direction> NextDirectionHistory = new List<Direction>();
    private Direction previousDirection;
    private List<Direction> PreviousDirectionHistory = new List<Direction>();
    

    //[SerializeField]
    private List<Transform> Tiles = null;


        
    int lastRowUsed = 0;
    /// <summary>
    /// Find the last row used and have that as the current row. 
    /// </summary>
    public void AddTile()
    {
        randomDirection = PickRandomDirection(lastRowUsed);

        TilePlace(ref lastRowUsed, randomDirection);

        TileConnect();
    }

    //Removes the first tile from the list
    public void RemoveTile()
    {
        Destroy(Tiles[0].gameObject);
        Tiles.RemoveAt(0);
    }

    private void Awake ()
    {
        Tiles = GetComponentInChildren<TileManager>().Tiles;//Using this Tiles being a pointer, now pointing at the Tile Managers Tiles.List


        int currentRow = 5;//Middle Row

        //intro start straight row
        for (int index = 0; index < 5; index++)
        {
            GameObject startTile = Instantiate(tilePrefab, new Vector3(0, 0, 2.54f * index), this.transform.rotation);//Starting Tile

            startTile.transform.parent = parentObject.transform;

            Tiles.Add(startTile.transform);//Adding the start tile to the Tiles list

            previousTile = startTile;//update the previous tile

            //Add forward index times
            NextDirectionHistory.Add(Direction.FORWARD);
        }

        randomDirection = Direction.FORWARD;
        
        for (int index = 1; index < 25; index++)
        {
         
            randomDirection = PickRandomDirection(currentRow);
               
            TilePlace(ref currentRow, randomDirection);

        }

        lastRowUsed = currentRow;
    }



    private void Start()
    {
        TileConnect();
    }



    /// <summary>
    /// This will pick a random direction for the next tile, calculating a new posistion in a way.
    /// </summary>
    /// <param name="currentRow"> Keeping track on what row it is in, valid row values(1,2,3) </param>
    private Direction PickRandomDirection(int currentRow)
    {
        //DIRECTION
        //work out which row last tile was in and calculate the next tile's direction
        if (currentRow == 1)
        {
            //generate random direction using row1_Directions
            int directionSelection = Random.Range(0, row1_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;
            PreviousDirectionHistory.Add(previousDirection);//The previous direction

            
            //Assign the next direction a random direction fron the currentRow
            randomDirection = row1_Directions[directionSelection];
            NextDirectionHistory.Add(randomDirection);//Random direction is the mew direction


            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);

        }
        else if (currentRow >= 2 && currentRow <= 9)
        {
            //generate random direction using row2_Directions
            int directionSelection = Random.Range(0, row2_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;
            PreviousDirectionHistory.Add(previousDirection);//The previous direction

            //Assign the next direction a random direction fron the currentRow
            randomDirection = row2_Directions[directionSelection];
            NextDirectionHistory.Add(randomDirection);//Random direction is the mew direction

            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);
        }
        else if (currentRow == 10)
        {
            //generate random direction using row3_Directions
            int directionSelection = Random.Range(0, row3_Directions.Length);

            //Setting the previous direction
            previousDirection = randomDirection;
            PreviousDirectionHistory.Add(previousDirection);//The previous direction
            
            //Assign the next direction a random direction fron the currentRow
            randomDirection = row3_Directions[directionSelection];
            NextDirectionHistory.Add(randomDirection);//Random direction is the mew direction

            //Debug.Log(currentRow + " : " + index + " : direction selection: " + directionSelection + " : " + nextDirection);
        }
        else
        {
            //something went wrong
            Debug.Log("WTF", this);
        }

        return randomDirection;

    }

    /// <summary>
    /// Placing a tile at in the row selected by the PickRandomDirection.
    /// </summary>
    /// <param name="rowNumber"> Keeping track on what row it is in, valid row values(1,2,3) </param>
    private void TilePlace(ref int rowNumber, Direction nextDirection)
    {
        float tileOffset = 2.54f;
        GameObject newTile = null;
        ////spawn a new tile in that direction AND update row
        if (nextDirection == Direction.FORWARD || (nextDirection == Direction.LEFT && previousDirection == Direction.RIGHT) || (nextDirection == Direction.RIGHT && previousDirection == Direction.LEFT))
        {
            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x, Vector2.zero.y, previousTile.transform.position.z + tileOffset), this.transform.rotation);//Straight along Z+
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile;//update the previous tile

        }
        else if (nextDirection == Direction.LEFT)
        {

            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x - tileOffset, Vector2.zero.y, previousTile.transform.position.z), this.transform.rotation);//Straight along X-
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile;//update the previous tile

            rowNumber--;//Adjust row

        }
        else if (nextDirection == Direction.RIGHT)
        {

            newTile = Instantiate(tilePrefab, new Vector3(previousTile.transform.position.x + tileOffset, Vector2.zero.y, previousTile.transform.position.z), this.transform.rotation);//Straight along X+
            newTile.transform.parent = parentObject.transform;

            previousTile = newTile;//update the previous tile

            rowNumber++;//Adjust row
        }

        //Adding a new tile to the Tiles list
        Tiles.Add(newTile.transform);

        
    }





    private void TileConnect()
    {

        //get comp???

        
        Debug.Log("Hmm");
        //TileInformation tileInfo = new TileInformation();//Tile Information

        //tileInfo.NextTile = newTile;//next tile pointer (kinda)
        //tileInfo.PreviousTile = previousTile;// previous tile (kinda)

        for (int index = 0; index < Tiles.Count; index++)
        {
            TileInformation currentTile = Tiles[index].GetComponent<TileInformation>();


            if (index == 0)//Start
            {
                currentTile.NextTile = Tiles[index + 1];
                currentTile.NextTileDirection = NextDirectionHistory[index + 1];
            }
            else if (index == Tiles.Count - 1)//End
            {
                currentTile.PreviousTile = Tiles[index - 1];
                currentTile.PreviousTileDirection = PreviousDirectionHistory[index - 1];
            }
            else//Everything else in the middle
            {
                currentTile.PreviousTile = Tiles[index - 1];
                currentTile.PreviousTileDirection = PreviousDirectionHistory[index - 1];

                currentTile.NextTile = Tiles[index + 1];
                currentTile.NextTileDirection = NextDirectionHistory[index + 1];
            }

            //currentTile.currentTileDirection = NextDirectionHistory[index];
        }


    }

}//end of class




public enum Direction
{
    FORWARD,
    LEFT,
    RIGHT
}