﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoRunForward : MonoBehaviour {

	[Range(0.0f, 4.0f)]
	public float Velocity;

 


	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{

		
		this.transform.Translate(Vector3.right * Velocity * Time.deltaTime);


	}
}
