﻿using UnityEngine;
using System.Collections;

public class RoomGenerator  : MonoBehaviour 
{
	private MapGenerator mapGenerator;

	private int randNum_MIN = 1;
	private int randNum_MAX = 5;
 
	// Use this for initialization
	void Start () 
	{
		//mapGenerator = GetComponent<MapGenerator> ();
		//Debug.Log (mapGenerator);


	}
	
	// Update is called once per frame
	void Update () 
	{


	
	}//End of Update

 

 
	public void RandomRoomGeneration() {
		mapGenerator = GetComponent<MapGenerator> ();

		int startRow = Random.Range (2, mapGenerator.mapRows - 7);
		int startColumn = Random.Range (2, mapGenerator.mapColumns - 7);
		// ╔ ═ ╗ ║ ╚ ╝ Walls
		//(Up Entry ˄)(Left Entry ˂)(Right Entry ˃)(Down Entry ˅)


		int roomNumber;

		roomNumber = Random.Range (randNum_MIN, randNum_MAX);

		Debug.Log ("RoomNumber = " + roomNumber);

		switch (roomNumber)
		{
			//Room size 3x3
			case 1:
				{
					room3x3VariationMethod (roomNumber, startRow, startColumn);
					break;
				}
				//Room Size 6x4 - Hori
			case 2:
				{
					//Debug.Log ("Making Room 2");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '╤';
					mapGenerator.map [startRow, startColumn + 2] = '╤';
					mapGenerator.map [startRow, startColumn + 3] = '╤';
					mapGenerator.map [startRow, startColumn + 4] = '╤';
					mapGenerator.map [startRow, startColumn + 5] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '˂';
					mapGenerator.map [startRow + 1, startColumn + 1] = '@';
					mapGenerator.map [startRow + 1, startColumn + 2] = '@';
					mapGenerator.map [startRow + 1, startColumn + 3] = '#';
					mapGenerator.map [startRow + 1, startColumn + 4] = '@';
					mapGenerator.map [startRow + 1, startColumn + 5] = '˃';

					mapGenerator.map [startRow + 2, startColumn] = '╚';
					mapGenerator.map [startRow + 2, startColumn + 1] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 2] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 3] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 4] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 5] = '╝';
					break;
				}

				//Room Size 3x7 - Vert
			case 3:
				{
					//Debug.Log ("Making Room 3");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '˄';
					mapGenerator.map [startRow, startColumn + 2] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '╟';
					mapGenerator.map [startRow + 1, startColumn + 1] = '@';
					mapGenerator.map [startRow + 1, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╟';
					mapGenerator.map [startRow + 2, startColumn + 1] = '@';
					mapGenerator.map [startRow + 2, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 3, startColumn] = '╟';
					mapGenerator.map [startRow + 3, startColumn + 1] = '#';
					mapGenerator.map [startRow + 3, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 4, startColumn] = '╟';
					mapGenerator.map [startRow + 4, startColumn + 1] = '@';
					mapGenerator.map [startRow + 4, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 5, startColumn] = '╟';
					mapGenerator.map [startRow + 5, startColumn + 1] = '@';
					mapGenerator.map [startRow + 5, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 6, startColumn] = '╚';
					mapGenerator.map [startRow + 6, startColumn + 1] = '˅';
					if ("X".Contains (mapGenerator.map [startRow + 7, startColumn + 1].ToString ()) && "˅".Contains (mapGenerator.map [startRow + 6, startColumn + 1].ToString ()))
					{
						Debug.Log ("Down is X! AND is roomExitDown Special spot at " + startRow + "," + startColumn);
						mapGenerator.map [startRow + 6, startColumn + 1] = '╧';
					}

					mapGenerator.map [startRow + 6, startColumn + 2] = '╝';
					break;
				}

				//Room Size 4x4
			case 4:
				{
					//Debug.Log ("Making Room 4");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '╤';
					mapGenerator.map [startRow, startColumn + 2] = '˄';
					mapGenerator.map [startRow, startColumn + 3] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '╟';
					mapGenerator.map [startRow + 1, startColumn + 1] = '@';
					mapGenerator.map [startRow + 1, startColumn + 2] = '@';
					mapGenerator.map [startRow + 1, startColumn + 3] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╟';
					mapGenerator.map [startRow + 2, startColumn + 1] = '#';
					mapGenerator.map [startRow + 2, startColumn + 2] = '@';
					mapGenerator.map [startRow + 2, startColumn + 3] = '╢';

					mapGenerator.map [startRow + 3, startColumn] = '╚';
					mapGenerator.map [startRow + 3, startColumn + 1] = '˅';
					mapGenerator.map [startRow + 3, startColumn + 2] = '╧';
					mapGenerator.map [startRow + 3, startColumn + 3] = '╝';
					break;
				}

				//Room size 5x5
			case 5:
				{
					//Debug.Log ("Making Room 5");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '╤';
					mapGenerator.map [startRow, startColumn + 2] = '˄';
					mapGenerator.map [startRow, startColumn + 3] = '╤';
					mapGenerator.map [startRow, startColumn + 4] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '╟';
					mapGenerator.map [startRow + 1, startColumn + 1] = '@';
					mapGenerator.map [startRow + 1, startColumn + 2] = '@';
					mapGenerator.map [startRow + 1, startColumn + 3] = '@';
					mapGenerator.map [startRow + 1, startColumn + 4] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╟';
					mapGenerator.map [startRow + 2, startColumn + 1] = '@';
					mapGenerator.map [startRow + 2, startColumn + 2] = '#';
					mapGenerator.map [startRow + 2, startColumn + 3] = '@';
					mapGenerator.map [startRow + 2, startColumn + 4] = '╢';

					mapGenerator.map [startRow + 3, startColumn] = '╟';
					mapGenerator.map [startRow + 3, startColumn + 1] = '@';
					mapGenerator.map [startRow + 3, startColumn + 2] = '@';
					mapGenerator.map [startRow + 3, startColumn + 3] = '@';
					mapGenerator.map [startRow + 3, startColumn + 4] = '╢';

					mapGenerator.map [startRow + 4, startColumn] = '╚';
					mapGenerator.map [startRow + 4, startColumn + 1] = '╧';
					mapGenerator.map [startRow + 4, startColumn + 2] = '˅';
					mapGenerator.map [startRow + 4, startColumn + 3] = '╧';
					mapGenerator.map [startRow + 4, startColumn + 4] = '╝';
					break;
				}


		}//End of Switch

	

		Debug.Log("Rooms created");
	}//End of RandomRoomSelection

	void room3x3VariationMethod(int roomNumber, int startRow, int startColumn)
	{
		randNum_MIN = 1;
		randNum_MAX = 3;

		roomNumber = 0;

		roomNumber = Random.Range (randNum_MIN, randNum_MAX);
		Debug.Log ("RoomNumber = " + roomNumber);

		switch (roomNumber)
		{
			//Room size 3x3 - Variation 1
			case 1:
				{
					//Debug.Log ("Making Room 1");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '˄';
					mapGenerator.map [startRow, startColumn + 2] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '╟';
					mapGenerator.map [startRow + 1, startColumn + 1] = '#';
					mapGenerator.map [startRow + 1, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╚';
					mapGenerator.map [startRow + 2, startColumn + 1] = '˅';
					mapGenerator.map [startRow + 2, startColumn + 2] = '╝';
					break;
				}

				//Room size 3x3 - Variation 2
			case 2:
				{
					//Debug.Log ("Making Room 1");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '˄';
					mapGenerator.map [startRow, startColumn + 2] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '╟';
					mapGenerator.map [startRow + 1, startColumn + 1] = '#';
					mapGenerator.map [startRow + 1, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╚';
					mapGenerator.map [startRow + 2, startColumn + 1] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 2] = '╝';
					break;
				}
				//Room size 3x3 - Variation 3
			case 3:
				{
					//Debug.Log ("Making Room 1");
					mapGenerator.map [startRow, startColumn] = '╔';
					mapGenerator.map [startRow, startColumn + 1] = '╤';
					mapGenerator.map [startRow, startColumn + 2] = '╗';

					mapGenerator.map [startRow + 1, startColumn] = '˂';
					mapGenerator.map [startRow + 1, startColumn + 1] = '#';
					mapGenerator.map [startRow + 1, startColumn + 2] = '╢';

					mapGenerator.map [startRow + 2, startColumn] = '╚';
					mapGenerator.map [startRow + 2, startColumn + 1] = '╧';
					mapGenerator.map [startRow + 2, startColumn + 2] = '╝';
					break;
				}
		}


	}
}//End of class
