﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Entity : MonoBehaviour {



    
    // Use this for initialization
    private void Start()
    {

    }

    // Update is called once per frame
    private void Update()
    {

    }

    protected virtual void Greeting()
    {
        Debug.Log("Basic Greeting!");
    }

    private void OnTriggerEnter(Collider other)
    {
       //Polymorphic Variable
        Entity entity = other.GetComponent<Entity>();

        if (entity != null)
        {
            entity.Greeting();
        }


    }
        

}

