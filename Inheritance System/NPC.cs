﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : NonHostile {

    GameObject Target;
    private void Awake()
    {
        Target = null;
    }
    // Use this for initialization
    void Start ()
    {
        Target = GameObject.FindGameObjectWithTag("Player");
    }
    
    // Update is called once per frame
    void Update ()
    {
        transform.LookAt(Target.transform);
    }

    //NPC's Greeting - Overrides the NonHostile Greeting
    protected override void Greeting()
    {
        //base.Greeting();
        Debug.Log("What would you like to buy today?");
    }
}
